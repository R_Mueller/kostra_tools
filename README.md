# README

## Kostra-Tools
This QGIS plug-in is for modelling different design floods with the ArcEGMO model of the federal state of Saxony-Anhalt
and the DWD KOSTRA and Rewanus data bases.
The plug-in was created on behalf on the 
Landesbetrieb für Hochwasserschutz und Wasserwirtschaft Saxony-Anhalt (LHW Sachsen-Anhalt).

The software needs the precipitation-runoff model ArcEGMO and suitable shape files with the KOSTRA and Rewanus data sets
to be fully functional.
For the configuration of the plug-in, set the setup__.py file in the SETUP folder accordingly.

This Software is designed as a plug-in for QGIS 3.xx.

## Author
Ruben Müller
E-Mail: ruben.mueller@bah-berlin.de
Company: Büro für Angewandte Hydrologie, Berlin
web: www.bah-berlin.de

## Licence
This Software is provided under the GPL3.
